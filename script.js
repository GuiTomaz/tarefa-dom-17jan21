var msgHistory = document.getElementById("history");
var userName = document.getElementById("userName");
var userMessage = document.getElementById("message");
var buttonSend = document.getElementById("send");
var buttonDeleteAll = document.getElementById("delete-all");

var listItems = [];

let i = 0;

/*  MVP3
    OBS: Quando eu vi no Discord no sábado a noite que o projeto era para ser decomposto em branches eu já tinha terminado 
    toda a parte em HTMl em 1 branch só. Então, para ficar de acordo com a tarefa separei o projeto em branches
    mesmo depois de já estar pronto. 
*/

//Listener do botão Enviar
buttonSend.addEventListener("click", function () {
    if (userName.value === "" || userMessage.value === "") {
        alert("Um dos campos está incompleto, escreva sua mensagem e envie novamente.");
        return;
    }
    //Cria um novo card
    var newCard = document.createElement("div");
    newCard.id = "div" + i;
    newCard.className = "div card";

    //Cria  e configura um botão "Delete" para o card da mensagem
    var newButtonDelete = document.createElement("button");
    newButtonDelete.id = "btnD" + i;
    newButtonDelete.innerHTML = `Deletar`;
    newButtonDelete.className = "btn delete";


    //Cria e configura um botão "edit" para o card da mensagem
    var newButtonEdit = document.createElement("button");
    newButtonEdit.id = "btnE" + i;
    newButtonEdit.innerHTML = `Editar`
    newButtonEdit.className = "btn edit";
    //Cria um paragrafo para colocarmos o nome do autor d amsg
    var nameString = document.createTextNode(userName.value);
    nameString.id = "autor" + i;
    //Span para tratamento do nome do autor da mensagem
    var spanNameString = document.createElement("span");
    spanNameString.appendChild(nameString);
    //Cria um item de uma lista cujo conteudo sera o nosso card
    var itemList = document.createElement("li");
    itemList.id = "li" + i;
    var msgString = document.createTextNode(userMessage.value);
    //Paragrafo para tratamento da mensagem
    var paragMessage = document.createElement("p");
    paragMessage.appendChild(msgString);

    msgString.id = "msg" + i
    //Acrescenta o nome do autor e o conteudo da mensagem no card

    newCard.appendChild(newButtonEdit);
    newCard.appendChild(newButtonDelete);
    newCard.appendChild(spanNameString);
    newCard.appendChild(document.createElement("br"));
    newCard.appendChild(paragMessage);


    //Finalmente coloca os botões e as mensagens dentro da div que é o nosso card

    itemList.appendChild(newCard);


    //Coloca o card na lista
    msgHistory.appendChild(itemList);

    //Listener para os botões Editar e Deletar
    newButtonDelete.addEventListener("click", function () {
        console.log("Lista length:" + listItems.length);
        let indexDivRemov = listItems.findIndex(function (element) {
            return element.id === itemList.id;
        });
        listItems[indexDivRemov] = listItems[listItems.length - 1];
        itemList.remove()
        listItems.pop();
    });
    newButtonEdit.addEventListener("click", () => edit(newCard, nameString, msgString));

    //Adiciona a div numa lista de divs
    listItems.push(itemList);
    //Reseta os campos
    userMessage.value = "";
    userName.value = "";
    i++;

});

//Listener do Botão "Excluir todos"
buttonDeleteAll.addEventListener("click", () => removeAll());

function removeAll() {
    //Remove todas as mensagens
    for (let d = 0; d <= listItems.length; d++) {
        if (listItems[d] != null) {
            let item = listItems[d]
            item.remove();
        }
    }
    listItems = [];
    i = 0;
}



function edit(card, nomeUser, msgUser) {
    //Cria uma div para conter o modo de edição e seus elementos
    var divEdit = document.createElement("div");
    var labelName = document.createElement("label");
    var textboxName = document.createElement("input");
    var buttonEdit = document.createElement("button");
    var labelContent = document.createElement("label");
    var textareaMessage = document.createElement("textarea");
    buttonEdit.className = "send";
    divEdit.className = "div card editC";
    //Configurando textarea
    textareaMessage.id = "edit-mode-message";
    textareaMessage.rows = "4";
    textareaMessage.cols = "50";
    textareaMessage.value = msgUser.textContent;
    //Configurando label da textarea
    labelContent.for = "edit-mode-message";
    labelContent.innerHTML = "Editar mensagem: ";

    //Configurando textbox
    textboxName.id = "edit-mode-userName";
    textboxName.type = "text";
    textboxName.value = nomeUser.textContent;

    //Configurando label do textobox
    labelName.for = "edit-mode-userName";
    labelName.innerHTML = "Editar nome: ";

    //Adicionando na div
    divEdit.appendChild(labelName);
    divEdit.appendChild(textboxName);
    divEdit.appendChild(document.createElement("br"));
    divEdit.appendChild(labelContent);
    divEdit.appendChild(textareaMessage);
    divEdit.appendChild(document.createElement("br"));
    divEdit.appendChild(document.createElement("br"));
    divEdit.appendChild(buttonEdit);

    //Configurando botão
    buttonEdit.innerHTML = "Editar";
    buttonEdit.addEventListener("click", () => {
        var autor = nomeUser;
        var conteudo = msgUser;
        autor.textContent = document.getElementById("edit-mode-userName").value;
        conteudo.textContent = document.getElementById("edit-mode-message").value;
        divEdit.remove();
    });

    //Adicionando no card
    card.appendChild(divEdit);
}